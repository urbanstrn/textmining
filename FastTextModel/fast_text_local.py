# latest development version of fasttext installed from github repository
# https://github.com/facebookresearch/fastText/tree/master/python

# import library
import fasttext

#load pre-trained model
model = fasttext.load_model("FastTextUnsupervised.bin")

# show 10 most similar words to word 'izpit'
print(model.get_nearest_neighbors("izpit", k=10))